use std::time::Duration;
use git2::build::CheckoutBuilder;

#[tokio::main]
async fn main() {
    let repository = git2::Repository::open("test_master").unwrap();

    let mut remote = repository.find_remote("origin").unwrap();

    let mut current_head = repository.head().unwrap().peel_to_commit().unwrap().id();

    loop {
        remote.fetch(&["master"], None, None).unwrap();

        let fetch_head = repository.find_reference("FETCH_HEAD").unwrap();
        repository.set_head(fetch_head.name().unwrap()).unwrap();
        repository.checkout_head(Some(CheckoutBuilder::default().force())).unwrap();
        if repository.head().unwrap().peel_to_commit().unwrap().id() != current_head {
            let output = std::process::Command::new("/home/edward/git/minici/on_change").output();
            println!("{:?}", String::from_utf8(output.unwrap().stdout).unwrap());
            current_head = repository.head().unwrap().peel_to_commit().unwrap().id();
        }
        tokio::time::sleep(Duration::from_secs(5)).await;
    }
}
